<?php namespace ICalHelper;

use Eluceo\iCal\Component\Calendar;
use Eluceo\iCal\Component\Timezone;
use Eluceo\iCal\Component\Event;
use Eluceo\iCal\Component\Alarm;

/**
 * Class Helper
 *
 * @package ICalHelper
 */
class Helper
{
    /**
     * Create an alarm
     *
     * @param $interval string
     * @return \Eluceo\iCal\Component\Alarm
     */
    public static function createAlarm($interval = '-PT1H')
    {
        $alarm = new Alarm();
        $alarm->setAction(Alarm::ACTION_DISPLAY)
             ->setTrigger($interval, true)
       ;

        return $alarm;
    }

    /**
     * Create an event
     *
     * @param $tz string
     * @param $date string
     * @param $time string
     * @param $summary string
     * @param $location string
     * @param $lat string
     * @param $long string
     * @param $organizer string
     * @return \Eluceo\iCal\Component\Event
     */
    public static function createEvent($tz, $date, $time, $summary, $location, $lat, $long, $organizer)
    {
        $event = new Event();

        $start  = new \DateTime("$date $time", new \DateTimeZone($tz));

        $event->setDtStart(clone($start))
              ->setDtEnd($start->add(\DateInterval::createFromDateString("1 hour")))
              ->setSummary($summary)
              ->setLocation($location, '', "$lat,$long")
              ->setUseTimezone(true)
              ->setOrganizer($organizer)
        ;

        return $event;
    }

    /**
     * Create a calendar
     *
     * @param $tz string
     * @param $url string
     * @return \Eluceo\iCal\Component\Calendar
     */
    public static function createCalendar($tz, $url)
    {
        $calendar = new Calendar($url);
        $calendar->setTimezone(new Timezone($tz));

        return $calendar;
    }

    /**
     * Build a calendar with an event and an alarm.
     *
     * @param $tz string
     * @param $date string
     * @param $time string
     * @param $summary string
     * @param $location string
     * @param $lat string
     * @param $long string
     * @param $organizer string
     * @return \Eluceo\iCal\Component\Calendar
     */
    public static function buildCalendar($tz, $url, $date, $time, $summary, $location, $lat, $long, $organizer)
    {
        $calendar = static::createCalendar($tz, $url);
        $event    = static::createEvent($tz, $date, $time, $summary, $location, $lat, $long, $organizer);
        $alarm    = static::createAlarm('-PT1H');

        $event->addComponent($alarm);
        $calendar->addComponent($event);

        return $calendar;
    }
}
